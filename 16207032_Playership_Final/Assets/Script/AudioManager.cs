﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioClip playerShootSound, enemyShootSound, explosion,winning,loser;
    public static AudioSource audioSource;
    
    void Start()
    {
        playerShootSound = Resources.Load<AudioClip>("playerShootSound");
        enemyShootSound = Resources.Load<AudioClip>("enemyShootSound");
        explosion = Resources.Load<AudioClip>("explosion");
        winning = Resources.Load<AudioClip>("winning");
        loser = Resources.Load<AudioClip>("loser");

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
            
    }

    public static void PlaySound(string clip)
    {

        switch (clip)
        {
            case "enemyShootSound" :
                audioSource.volume = 0.1f;
                audioSource.PlayOneShot(enemyShootSound);
                break;
            case "playerShootSound" :
                audioSource.volume = 0.1f;
                audioSource.PlayOneShot(playerShootSound);
                break;
            case "explosion" :
                audioSource.volume = 0.1f;
                audioSource.PlayOneShot(explosion);
                break;
            case "winning" :
                audioSource.volume = 0.01f;
                audioSource.PlayOneShot(winning);
                break;
            case "loser" :
                audioSource.volume = 0.006f;
                audioSource.PlayOneShot(loser);
                break;
        }
        
    }
}
