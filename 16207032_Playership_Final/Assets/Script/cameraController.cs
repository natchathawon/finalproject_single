﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{
    public Transform playerTransform;
    private Vector3 cameraOffset;
    [Range(0.1f,1.0f)]
    public float smoothFactor = 0.5f;
    void Start()
    {
        cameraOffset = transform.position - playerTransform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 newpos = playerTransform.position + cameraOffset;
        transform.position = Vector3.Slerp(transform.position, newpos, smoothFactor);
    }
}
