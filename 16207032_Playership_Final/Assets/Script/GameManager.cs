﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static bool isplayerDeath = false;
    public static bool isBossDeath = false;
    public static int playerScore;
    private float timeCounter;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       SceneChanger();
    }

    void SceneChanger()
    {
        if (isBossDeath == true)
        {
            timeCounter += Time.deltaTime;
            AudioManager.PlaySound("winning");
            if (timeCounter > 5.0f)
            {
                ResetGame();
                SceneManager.LoadScene("WinnerScene");
            }
        }

        if (isplayerDeath == true)
        {
            timeCounter += Time.deltaTime;
            AudioManager.PlaySound("loser");
            if (timeCounter > 5.0f)
            {
                ResetGame();
                SceneManager.LoadScene("GameOver");
            }
        }
        
        if (playerScore == 5)
        {
            timeCounter += Time.deltaTime;
            AudioManager.PlaySound("winning");
            if (timeCounter > 5.0f)
            {
                ResetGame();
                SceneManager.LoadScene("BossScene");
            }
        }
        
        

    }

    public void LoadMenu()
    {
        ResetGame();
        SceneManager.LoadScene("Menu");
    }
    
    public void LoadGameplay()
    {
        ResetGame();
        SceneManager.LoadScene("Gameplay1Scene");
    }

    void ResetGame()
    {
        playerScore = 0;
        isplayerDeath = false;
        isBossDeath = false;
    }
    
    
    
}
