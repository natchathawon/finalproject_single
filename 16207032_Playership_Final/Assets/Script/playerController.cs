﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{
    // Start is called before the first frame update
    private float horizon, vertical;
    [SerializeField] float speed;
    public GameObject playerBullet;
    public float playerHealth = 5;
    public Text healthText;

    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = "HP = " + playerHealth;
        PlayerMove();
        PlayerShoot();
        Death();
    }

    void PlayerMove()
    {
        horizon = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        float translationX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float translationY = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        transform.Translate(translationX, translationY, 0);
    }

    void PlayerShoot()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(playerBullet, transform.position, transform.rotation);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "enemyBullet")
        {
            playerHealth--;
            Destroy(other.gameObject);
            AudioManager.PlaySound("explosion");
        }
        
        if (other.gameObject.tag == "enemyBulletBoss")
        {
            playerHealth -= 5;
            Destroy(other.gameObject);
           
        }

    }

    void Death()
    {
        if (playerHealth <= 0)
        {
            AudioManager.PlaySound("explosion");
            GameManager.isplayerDeath = true;
            Destroy(this.gameObject);
        }
    }
}




