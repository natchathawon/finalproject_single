﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class enemieController : MonoBehaviour
{
    private float hp = 3;
    private float timeCounter;
    private bool weaponReady = false;
    public GameObject enemyBullet;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeCounter();
        FireBullet();
        EnemiesController();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "playerBullet")
        {
            Destroy(other.gameObject);
            AudioManager.PlaySound("explosion");
            hp--;
        }
        
        if (other.gameObject.tag == "Player")
        {
            AudioManager.PlaySound("explosion");
            Destroy(other.gameObject);
            GameManager.isplayerDeath = true;
            GameManager.playerScore = 0;

        }
        
    }

    void EnemiesController()
    {
        if (hp == 0)
        {
            AudioManager.PlaySound("explosion");
            GameManager.playerScore++;
            Destroy(this.gameObject);
        }
    }

    void TimeCounter()
    {

            timeCounter += Time.deltaTime;
            if (timeCounter > 2.5f)
            {
                weaponReady = true;
                FireBullet();
            }
    }

    void FireBullet()
    {
        if (weaponReady == true)
        {
            weaponReady = false;
            timeCounter = 0;
            AudioManager.PlaySound("enemyShootSound");
            Instantiate(enemyBullet, transform.position, transform.rotation);
        }
    }
    
    
}
