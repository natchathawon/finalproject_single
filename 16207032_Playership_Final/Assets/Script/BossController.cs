﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    private float hp = 20;
    private float timeCounter;
    private bool weaponReady = false;
    public GameObject enemyBullet, enemyBulletBoss;
    private float bulletChance;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TimeCounter();
        FireBullet();
        EnemiesController();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "playerBullet")
        {
            Destroy(other.gameObject);
            AudioManager.PlaySound("explosion");
            hp--;
        }
        
        if (other.gameObject.tag == "Player")
        {
            AudioManager.PlaySound("explosion");
            Destroy(other.gameObject);
            GameManager.isplayerDeath = true;
            GameManager.playerScore = 0;

        }
        
    }

    void EnemiesController()
    {
        if (hp == 0)
        {
            AudioManager.PlaySound("explosion");
            Destroy(this.gameObject);
            GameManager.isBossDeath = true;
        }
    }

    void TimeCounter()
    {

        timeCounter += Time.deltaTime;
        if (timeCounter > 2.5f)
        {
            weaponReady = true;
            bulletChance = Random.Range(1, 8);
            FireBullet();
        }
    }

    void FireBullet()
    {
        if (weaponReady == true)
        {
            if (bulletChance >= 6)
            {
                weaponReady = false;
                timeCounter = 0;
                AudioManager.PlaySound("enemyShootSound");
                Instantiate(enemyBulletBoss, transform.position, transform.rotation);
            }
            else
            {
                weaponReady = false;
                timeCounter = 0;
                AudioManager.PlaySound("enemyShootSound");
                Instantiate(enemyBullet, transform.position, transform.rotation);
            }
            
        }
    }

}
